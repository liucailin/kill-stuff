﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MoveData.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace KillStuff.Logic.Features.Input.Events
{
    public class MoveData
    {
        #region Properties

        /// <summary>
        ///   Direction to move.
        /// </summary>
        public MoveDirection Direction { get; set; }

        /// <summary>
        ///   Starting or stopping to move.
        /// </summary>
        public bool Enable { get; set; }

        /// <summary>
        ///   Entity to move.
        /// </summary>
        public int EntityId { get; set; }

        #endregion
    }
}