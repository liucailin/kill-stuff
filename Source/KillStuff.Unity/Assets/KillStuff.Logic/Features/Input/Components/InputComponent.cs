﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InputComponent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace KillStuff.Logic.Features.Input.Components
{
    using KillStuff.Logic.Features.Input.Events;

    using Slash.ECS.Components;
    using Slash.ECS.Inspector.Attributes;

    [InspectorComponent]
    public class InputComponent : EntityComponent
    {
        #region Properties

        /// <summary>
        ///   Current move directions.
        /// </summary>
        public MoveDirection MoveDirections { get; set; }

        #endregion
    }
}