﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LevelSystem.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace KillStuff.Logic.Features.Core.Systems
{
    using System;
    using System.Collections.Generic;

    using KillStuff.Logic.Features.Core.Components;
    using KillStuff.Logic.Features.Input.Components;
    using KillStuff.Logic.Features.Movement.Components;
    using KillStuff.Logic.Features.Weapons.Components;

    using Slash.Collections.AttributeTables;
    using Slash.ECS.Blueprints;
    using Slash.ECS.Events;
    using Slash.ECS.Systems;
    using Slash.Math.Algebra.Vectors;

    [GameSystem]
    public class LevelSystem : GameSystem
    {
        #region Constants

        private const string ProjectileBlueprintId = "Projectile";

        #endregion

        #region Public Methods and Operators

        public override void Init(IAttributeTable configuration)
        {
            base.Init(configuration);

            this.EventManager.RegisterListener(FrameworkEvent.GameStarted, this.OnGameStarted);
        }

        #endregion

        #region Methods

        private void CreatePlayerCharacter()
        {
            // Create player blueprint and instantiate the player character.
            var playerBlueprint = new Blueprint
            {
                ComponentTypes =
                    new List<Type>()
                    {
                        typeof(TransformComponent),
                        typeof(MovementComponent),
                        typeof(InputComponent),
                        typeof(WeaponComponent),
                        typeof(AnchorComponent),
                        typeof(VisualizationComponent)
                    }
            };

            var playerConfiguration = new AttributeTable
            {
                { WeaponComponent.AttributeProjectileBlueprintId, ProjectileBlueprintId },
                { WeaponComponent.AttributeBulletCount, 10 },
                { WeaponComponent.AttributeSpeed, 10.0f },
                { AnchorComponent.AttributeOffset, new Vector2F(0.9f, -0.537f) },
                { VisualizationComponent.AttributePrefab, "Survivor" }
            };

            // Create player entity.
            this.EntityManager.CreateEntity(playerBlueprint, playerConfiguration);
        }

        private void CreateProjectileBlueprint()
        {
            // Create and add projectile blueprint.
            var projectileBlueprint = new Blueprint()
            {
                ComponentTypes =
                    new List<Type>()
                    {
                        typeof(TransformComponent),
                        typeof(MovementComponent),
                        typeof(VisualizationComponent)
                    },
                AttributeTable = new AttributeTable() { { VisualizationComponent.AttributePrefab, "Projectile" } }
            };
            this.BlueprintManager.AddBlueprint(ProjectileBlueprintId, projectileBlueprint);
        }

        private void OnGameStarted(GameEvent e)
        {
            this.CreateProjectileBlueprint();
            this.CreatePlayerCharacter();
        }

        #endregion
    }
}