﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TransformBehaviour.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace KillStuff.Unity.Features.Movement.Behaviours
{
    using KillStuff.Logic.Features.Movement.Components;

    using Slash.Unity.Common.ECS;
    using Slash.Unity.Common.Math;

    public class TransformBehaviour : EntityComponentBehaviour<TransformComponent>
    {
        #region Methods

        protected void Update()
        {
            if (this.Component != null)
            {
                // Update transform.
                this.transform.position = this.Component.Position.ToVector3XY();
            }
        }

        #endregion
    }
}