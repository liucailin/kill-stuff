﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InputBehaviour.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace KillStuff.Unity.Features.Input.Behaviours
{
    using KillStuff.Logic.Features.Input.Components;
    using KillStuff.Logic.Features.Input.Events;

    using Slash.Unity.Common.ECS;

    using UnityEngine;

    public class InputBehaviour : EntityComponentBehaviour<InputComponent>
    {
        #region Methods

        protected void Update()
        {
            if (this.Component != null)
            {
                if (Input.GetButtonDown("Forward"))
                {
                    this.Move(MoveDirection.Forward, true);
                }
                if (Input.GetButtonUp("Forward"))
                {
                    this.Move(MoveDirection.Forward, false);
                }
                if (Input.GetButtonDown("Backward"))
                {
                    this.Move(MoveDirection.Backward, true);
                }
                if (Input.GetButtonUp("Backward"))
                {
                    this.Move(MoveDirection.Backward, false);
                }
                if (Input.GetButtonDown("Left"))
                {
                    this.Move(MoveDirection.Left, true);
                }
                if (Input.GetButtonUp("Left"))
                {
                    this.Move(MoveDirection.Left, false);
                }
                if (Input.GetButtonDown("Right"))
                {
                    this.Move(MoveDirection.Right, true);
                }
                if (Input.GetButtonUp("Right"))
                {
                    this.Move(MoveDirection.Right, false);
                }

                // Fire weapon.
                if (Input.GetButtonDown("Fire"))
                {
                    this.Entity.Game.EventManager.QueueEvent(InputAction.Fire, this.Entity.EntityId);
                }

                // Reload weapon.
                if (Input.GetButtonDown("Reload"))
                {
                    this.Entity.Game.EventManager.QueueEvent(InputAction.Reload, this.Entity.EntityId);
                }
            }
        }

        private void Move(MoveDirection direction, bool enable)
        {
            this.Entity.Game.EventManager.QueueEvent(
                InputAction.Move,
                new MoveData() { EntityId = this.Entity.EntityId, Direction = direction, Enable = enable });
        }

        #endregion
    }
}